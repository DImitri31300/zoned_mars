<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::view('/', 'welcome');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('zoned', 'ZonedController@zoned');
Route::get('zoned/create', 'ZonedController@create');
Route::post('zoned', 'ZonedController@show');

Route::get('ore', 'OreController@ore');
Route::get('ore/create', 'OreController@create');
Route::post('ore', 'OreController@show');

Route::get('profile/{id}', 'UserController@show')->middleware('auth');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
Auth::routes();
