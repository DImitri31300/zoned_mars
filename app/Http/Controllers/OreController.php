<?php

namespace App\Http\Controllers;

use App\Ore;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OreController extends Controller
{
    public function ore(Request $request)
    { 
        $ore = DB::table('ores')->get();

        return view('ore', [
            'ore' => $ore
        ]);
    }

    public function create()
    {
        $ores = Ore::all();

        return view('create.addore', compact('ores'));
    }

    public function show()
    {
         $data = request()->validate([
            'name'          => 'required',
            'comment'       => 'required',
            'image'         => 'required'
        ]);

            Ore::create($data);

            return redirect('ore');
    }
}
