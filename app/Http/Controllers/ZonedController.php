<?php

namespace App\Http\Controllers;

use App\Ore;
use App\Zoned;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ZonedController extends Controller
{
    public function zoned(Request $request)
    {
        $zone = DB::table('zoneds')->get();

        return view('zoned', [
            'zoned' => $zone
        ]);
    }

    public function create()
    {
        $zones = Zoned::all();
        $ores  = Ore::all();

        return view('create.addzoned', compact('zones','ores'));
    }

    public function show()
    {
         $data = request()->validate([
            'name'          => 'required|min:3',
            'coordinate'    => 'required|min:3',
            'ore'           => 'required',
            'danger'        => 'required|integer',
            'comment'       => 'required|min:3',
            'image'         => 'required|min:3'
        ]);

            Zoned::create($data);

            return redirect('zoned');
    }
}
