<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\Authenticate;


class UserController extends Controller
{
    public function show(Request $request, $id)
    {
        $members = User::all();

        return view('profile',compact('members'));
    }
}
