<?php

namespace App;

use App\Ore;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;


class Zoned extends Model
{
    protected $guarded = [];

    public function entreprise()
    {
        return $this->belongsTo('App\Ore');
    }
}
