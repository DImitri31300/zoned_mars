@extends('layouts.app')


@section('content')
<div align="center" class="container">
<h1>Présentation de l'application</h1>
  <div class="card">
    <div class="card-header">
      <h2>RECALL CORPORATE</h2><br>
      <img style="border-radius:5%;" src="https://media3.giphy.com/media/uU8ZXJAjwCy8mGSWsw/giphy.gif" alt="">
    </div>
    <div class="card-body">
      <h5 class="card-title">Bienvenue sur l'application coloniale Recall prévention. <br> Application ouverte à tous les colons des colonies martiennes afin de prévenir des zones à risque dans le domaine des gisements radioactif de la planète.</h5>
      <a href="/zoned" class="btn btn-danger">Protégez vous</a>
    </div>
  </div>
</div>

@endsection