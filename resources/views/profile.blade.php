@extends('layouts.app')

@section('content')
    <div class="container ">
        <h1 class="display-4">Nom : {{ Auth::user()->name }}</h1>
        <p>Colon Numéro {{ Auth::user()->id }} </p>
        <p>Votre E-mail : {{ Auth::user()->email }}</p>
        <p style="color:white;">Inscrit Depuis le : {{ Auth::user()->created_at }}</p>
    </div>
        {{-- <img src="{{ url('public/storage/app/image.jpg')"alt="avatar">-- }}

@endsection 