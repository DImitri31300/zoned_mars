@extends('layouts.app')

@section('content')
<h1 align="center">Partager une zone à risque</h1>
<form action="/zoned" method="POST" align="center" class="container">
    @csrf
    <div class="form-group">
        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Donnez un nom...">
        @error('name')
            <div class="invalid-feedback">
                {{ $errors->first('name') }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="coordinate" placeholder="Donnez une coordonée...">
    </div>
        <div class="form_group">
            <select class="custom-select" name="ore">
                @foreach($ores as $ore)
                <option value="{{ $ore->name }}">{{ $ore->name }}</option>
                @endforeach
            </select>
        </div>
        <br>
    <div class="form-group">
        <label style="color:white;font-size:20px;" for="danger">Niveau de dangerosité de la zone, estimé de 1 à 10...</label>
        <input type="range" id="danger" min="0" max="10" class="form-control" name="danger"  placeholder="Niveau de dangerosité de la zone, estimé de 1 à 10...">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="comment" placeholder="Donnez une description de la zone...">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="image" placeholder="Transmettez une image de la zone...">
    </div>
    <br>
    <button type="submit" class="btn btn-danger">Partagez votre découverte</button>
</form>
<br><br><br>

@endsection