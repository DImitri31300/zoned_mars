<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://images.alphacoders.com/475/475707.jpg" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"jpg>
    <link href="css/app.css" rel="stylesheet">
    <title>Mars</title>
</head>
<body style="   	background: url('https://www.itl.cat/pics/b/19/193869_planet-mars-wallpaper.jpg') -0px 0px no-repeat;background-size: cover;	background-attachment: fixed;">
  <header>
    <nav class="navbar navbar-expand-lg navbar-light "style="background-color: white;">   
        <a class="navbar-brand" href="/">
          <img src="/img/marz.png" alt="retour à l'accueil">
          <h3 class="navbar-brand">Recall Corporate</h3>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto ">
            <li class="nav-item active">
              <a class="nav-link" href="/">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/zoned">Les zones</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/ore">Les minerais</a>
            </li>
            @auth    
            <li class="nav-item">
                <a class="nav-link" href="/zoned/create">Déclarer un terrain à risque</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/ore/create">Déclarer un nouveau type de minerai</a>
                </li>    
            <li>
            @endauth
            @guest
          <form class="form-inline my-2 my-lg-0">
            <a class="nav-link btn btn-outline-danger my-2 my-sm-0" href="/login">ESPACE COLONS</a>
          </form>
              @if (Route::has('register'))
                  @endif
              @else
              <li class="nav-item dropdown" style="list-style:none;">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Déconnexion') }}
                      </a>
                      <a class="dropdown-item"  href="/profile/{id}">Mon espace</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
              @endguest
        </div>
      </nav>
      <div class="container">
      <main class="py-4">    
      </main>            
  </header>

  @yield('content')
</div>
    <!-- Footer -->
<footer class="page-footer  font-small  pt-4 " style="background-color: #000;">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h5 class="text-uppercase">Fédération des colons martiens</h5>
        <img src="/img/logo.jpg" width="200" height="200" alt="">
        <div class="rs_footer ">
            <a href="/">
                <img src="/img/marz.png" alt="retour à l'accueil">
            </a>
        </div>
      </div>
      <!-- Grid column -->
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- menu -->
        <h5 class="text-uppercase">Menu</h5>

        <ul class="list-unstyled">
          <li>
            <a href="/">Accueil</a>
          </li>
          <li>
            <a href="/zoned">Les zones</a>
          </li>
          <li>
            <a href="/ore">Les minerais</a>
          </li> 
          @auth       
          <li>
              <a href="/zoned/create">Déclarer un terrain à risque</a>
            </li>
            <li>
                <a href="/ore/create">Déclarer un nouveau type de minerai</a>
              </li>    
          <li>
          @endauth
            <a href="/login">Espace adhérents</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">Contact</h5>

        <ul class="list-unstyled">
          <li>
            <p><strong>Adresse :</strong></p>
            <p>Colonie Oméga place Phoebus</p>
          </li>
          <li>
            <p><strong>Téléphone :</strong></p>
            <p>019-N-345-T-432-U-23-I</p>
          </li>
          <li>
              <p><strong>Email :</strong></p>
            <p>colonsm@galax.uni</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
   <!-- Copyright -->
   <div  class="footer-copyright text-center py-3">
    <a style="font-size:15px" href="http://www.astrosurf.com/luxorion/mars-colonisation2.htm">© 2248 Fédération Colons martiens</a>
  </div>
</footer>
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
</body>

</html>