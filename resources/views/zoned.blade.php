@extends('layouts.app')


@section('content')
<div align="center" class="container">
    <h1>Zone de radioactivité de Mars</h1>

    <table class="table">
    <thead>
      <tr>
        <th scope="col">Nom de la zone</th>
        <th scope="col">Coordonnées</th>
        <th scope="col">Minerai</th>
        <th scope="col">Dangerosité</th>
        <th scope="col">Description</th>
        <th scope="col">Découverte</th>
        <th scope="col">Photo</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($zoned as $zone )
              <tr>
                <td>{{ $zone->name }}</td>
                <td>{{ $zone->coordinate }}</td>
                <td>{{ $zone->ore }}</td>
                <td>{{ $zone->danger }}</td>
                <td>{{ $zone->comment }}</td>
                <td>{{ $zone->created_at }}</td>
                <td><div ><img style="height:250px; width:450px;border-radius: 25% 10%;box-shadow: 10px 5px 5px red;" src="{{ $zone->image }}" alt=""></div></td>
              </tr>
          
              @endforeach
            </tbody>
          </table>   
</div>

@endsection