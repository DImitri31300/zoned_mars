@extends('layouts.app')

@section('content')
<div align="center" class="container">
<h1>Type de minerais, danger !</h1>


<table class="table">
<thead>
  <tr>
    <th scope="col">Nom du minerai</th>
    <th scope="col">Description</th>
    <th scope="col">Découverte</th>
    <th scope="col">photo</th>
  </tr>
</thead>
<tbody>
@foreach ($ore as $ores )
          <tr>
            <td>{{ $ores->name }}</td>
            <td>{{ $ores->comment }}</td>
            <td>{{ $ores->created_at }}</td>
            <td><img  style="height:250px; width:450px;border-radius: 25% 10%;box-shadow: 10px 5px 5px red;"src="{{ $ores->image }}" alt=""></td>
          </tr>
          @endforeach
        </tbody>
      </table>   
</div>

@endsection